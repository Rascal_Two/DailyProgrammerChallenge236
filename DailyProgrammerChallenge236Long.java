package challenge.easy236;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class TetrisPieceGenerator{
	private static Scanner	userInput		= new Scanner(System.in);
	private static char[]	pieces			= new char[] { 'O', 'I', 'S', 'Z', 'L', 'J', 'T' };
	private static int		piecesWanted	= 50;
	private static int		bagSize			= 7;
	
	public static void main(String[] args){
		if (args.length == 0) {
			mainMenu();
		}
		else{
			for (int i = 0; i < args.length; i++){
				if (args[i].equalsIgnoreCase("-pieceswanted")) {
					if (isNumber(args[i + 1])) {
						piecesWanted = Integer.parseInt(args[i + 1]);
					}
					else{
						System.out.println(args[i + 1] + " Is Not A Number!");
						return;
					}
				}
				else if (args[i].equalsIgnoreCase("-bagsize")) {
					if (isNumber(args[i + 1])) {
						bagSize = Integer.parseInt(args[i + 1]);
					}
					else{
						System.out.println(args[i + 1] + " Is Not A Number!");
						return;
					}
				}
				else if (args[i].equalsIgnoreCase("-pieces")) {
					pieces = args[i + 1].toCharArray();
				}
			}
			switch (args[0]){
				case "-g":
					generate(false);
					break;
				case "-v":
					verify(false, args[args.length - 1]);
					break;
				case "-h":
					System.out.println("     -Help Screen-");
					System.out.println("        -Modes-");
					System.out.println("-v                    Verification Mode. Enter String To Verify As Last Argument.");
					System.out.println("-g                    Generation Mode, Can Be Ran Alone Or With Settings");
					System.out.println("       -Settings-");
					System.out.println("-pieceswanted         Number Of Pieces You Want To Be Generated.");
					System.out.println("-pieces               Pieces That You Want To Be Generated.");
					System.out.println("-bagsize              Size Of Each Bag In Verification String.");
					break;
				default:
					System.out.println("Incorrect Action! Use -h To View Correct Actions & Help!");
					break;
			}
		}
	}

	private static void mainMenu(){
		System.out.println();
		System.out.println("Choose Action:");
		System.out.println("1. Generate Pieces");
		System.out.println("2. Verify Generated Pieces");
		switch (userInput.next()){
			case "1":
				generationMenu();
				break;
			case "2":
				verificationMenu();
				break;
			default:
				System.out.println("Invalid Selection");
				mainMenu();
				break;
		}
	}

	private static void generationMenu(){
		System.out.println();
		System.out.println("Tetris Piece Generator");
		System.out.println("1. Run");
		System.out.println("2. View/Change Settings");
		switch (userInput.next()){
			case "1":
				generate(true);
				break;
			case "2":
				System.out.println();
				System.out.println("Choose Setting You Wish To Change:");
				System.out.println("1. Pieces Wanted - " + piecesWanted);
				String bagContentString = "[";
				for (Character piece : pieces){
					bagContentString += piece + ", ";
				}
				bagContentString = bagContentString.substring(0, bagContentString.length() - 2) + "]";
				System.out.println("2. Bag Default Contents - " + bagContentString);
				System.out.println("3. Return To Generation Menu");
				switch (userInput.next()){
					case "1":{
						System.out.print("Enter The Amount Of Pieces You Want: ");
						String input = userInput.next();
						try{
							piecesWanted = Integer.valueOf(input);
							System.out.println("Pieces Wanted Changed To: " + piecesWanted);
						}
						catch (NumberFormatException ex){
							System.out.println(input + " Is Not A Valid Number!");
						}
						generationMenu();
						break;
					}
					case "2":{
						System.out.println("Enter The Default Bag Contents: ");
						pieces = userInput.next().toCharArray();
						generationMenu();
						break;
					}
					case "3":{
						generationMenu();
						break;
					}
					default:{
						System.out.println("Incorrect Selection");
						generationMenu();
						break;
					}
				}
				break;
			default:
				System.out.println("Invalid Selection");
				generationMenu();
				break;
		}
		
	}

	private static void generate(boolean menu){
		System.out.println("Input:");
		System.out.println("Pieces Per Bag: " + charArrayToString(pieces));
		System.out.println("Pieces Wanted: " + piecesWanted);
		System.out.println();
		List<Character> bag = new ArrayList<Character>(pieces.length);
		String output = "";
		Random random = new Random();
		for (int piecesGenerated = 0; piecesGenerated < piecesWanted; piecesGenerated++){
			if (!bag.isEmpty()) {
				output += bag.remove(random.nextInt(bag.size()));
			}
			else{
				fillBag(bag, pieces);
			}
		}
		System.out.println("Output:");
		System.out.println(output);
		if (menu) {
			mainMenu();
		}
	}

	private static void verificationMenu(){
		System.out.println();
		System.out.println("Tetris Generated Pieces Verification");
		System.out.println("1. Run");
		System.out.println("2. View/Change Settings");
		switch (userInput.next()){
			case "1":
				verify(true, null);
				break;
			case "2":
				System.out.println();
				System.out.println("Choose Setting You Wish To Change:");
				System.out.println("1. Bag Size - " + bagSize);
				System.out.println("2. Return To Verification Menu");
				switch (userInput.next()){
					case "1":
						System.out.print("Enter The Size Of Each Bag: ");
						String input = userInput.next();
						try{
							bagSize = Integer.valueOf(input);
							System.out.println("Default Bag Size Changed To: " + bagSize);
						}
						catch (NumberFormatException ex){
							System.out.println(input + " Is Not A Valid Number!");
						}
						verificationMenu();
						break;
					case "2":
						verificationMenu();
						break;
					default:
						System.out.println("Invalid Selection");
						verificationMenu();
						break;
				}
				break;
			default:
				System.out.println("Invalid Selection");
				verificationMenu();
				break;
		}
	}
	
	private static void verify(boolean menu, String toVerify){
		if (toVerify == null) {
			System.out.println("Enter Generated Pieces To Verify:");
			toVerify = userInput.next();
		}
		List<String> bags = new ArrayList<String>();
		String currentBag = "";
		System.out.println("Verifying " + toVerify + " With A Bag Size Of " + bagSize);
		
		for (int i = 0; i < toVerify.length(); i++){
			if (currentBag.length() < bagSize) {
				currentBag += toVerify.charAt(i);
			}
			else{
				bags.add(currentBag);
				currentBag = "" + toVerify.charAt(i);
			}
		}
		if (currentBag != "") {
			bags.add(currentBag);
		}
		List<String> badBags = new ArrayList<String>();
		for (String bag : bags){
			currentBag:
			for (Character piece : bag.toCharArray()){
				for (int i = 0; i < bag.length(); i++){
					if (bag.indexOf(piece) != i && piece.equals(bag.charAt(i))) {
						badBags.add(bag);
						break currentBag;
					}
				}
			}
		}
		bags.removeAll(badBags);
		if (badBags.isEmpty()) {
			System.out.println("The Generated Pieces Are Valid.");
		}
		else if (bags.isEmpty()) {
			System.out.println("The Generated Pieces Are NOT Valid.");
		}
		if (menu) {
			mainMenu();
		}
	}

	private static void fillBag(List<Character> bag, char[] pieces){
		for (char piece : pieces){
			bag.add(piece);
		}
	}

	/*
	 * Simple Methods
	 */

	public static String charArrayToString(char[] array){
		String string = "";
		for (char character : array){
			string += character;
		}
		return string;
	}

	public static boolean isNumber(String string){
		try{
			Integer.parseInt(string);
			return true;
		}
		catch (NumberFormatException ex){
			return false;
		}
	}
	
}
